## lore-tasks

Tasks involving https://lore.kernel.org, a https://public-inbox.org email archive for the Linux kernel

## Usage

```
❯ ./get-stable-rc-review -h
usage: get-stable-rc-review [-h] [--author AUTHOR] [--subject SUBJECT]

Get a stable rc review from lore

optional arguments:
  -h, --help         show this help message and exit
  --author AUTHOR    Email author
  --subject SUBJECT  Email subject
```

Does a number of git operations to find an email message in a `lore` git 
repository before printing the email to `stdout`. It leaves behind a directory,
 `stable.git`, which can be safely removed.

```
❯ ./build-stable-rc-response -h
usage: build-stable-rc-response [-h] review

Build a stable rc response from a stable rc review

positional arguments:
  review      Stable rc review email

optional arguments:
  -h, --help  show this help message and exit
```

Takes the email from `get-stable-rc-review` and builds an email response.

```
❯ ./print-email-headers -h
usage: print-email-headers [-h] [--headers HEADERS] email

Print email headers

positional arguments:
  email              Email

optional arguments:
  -h, --help         show this help message and exit
  --headers HEADERS  Print only these headers
```

Provides a way to both smoke test and validate an email message.

## Contributing
This (alpha) project is managed on `gitlab` at https://gitlab.com/Linaro/lkft/reports/lore-tasks

Open an issue at https://gitlab.com/Linaro/lkft/reports/lore-tasks/-/issues

Open a merge request at https://gitlab.com/Linaro/lkft/reports/lore-tasks/-/merge_requests

For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://gitlab.com/Linaro/lkft/reports/lore-tasks/-/blob/main/LICENSE)
