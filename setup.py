import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="lore-tasks",
    version="0.0.1",
    author="Justin Cook",
    author_email="justin.cook@linaro.org",
    description="lore tasks",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    scripts=["build-stable-rc-response", "get-stable-rc-review", "print-email-headers"],
    install_requires=["email_reply_parser", "semver"],
)
